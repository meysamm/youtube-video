<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\Video;
use Illuminate\Support\Facades\Storage;
use App\Library\Youtube;
//use Masih\YoutubeDownloader\YoutubeDownloader;

class VideoController extends Controller
{
    public function index()
    {
        $data = [
            'last' => Video::orderBy('id', 'desc')->take(8)->get(),
            'top' => Video::orderBy('counter', 'desc')->take(8)->get()
        ];
        return view('video.index', $data);
    }

    public function list($tag = null)
    {
        if( is_null($tag) || $tag == ''){
            $last = Video::orderBy('id', 'desc')->paginate(16);
        }
        else{
            $tag = Tag::whereSlug($tag)->first();
            if( !$tag ){
                abort(404);
            }
            $last = $tag->videos()->paginate(16);
        }
        $data = [
            'last' => $last,
        ];
        return view('video.list', $data);
    }

    public function detail($id)
    {
        $row = Video::findOrFail($id);
        $row->counter += 1; // increase counter data
        $row->save();
        $data = [
            'row' => $row
        ];
        return view('video.detail', $data);
    }

    public function submit()
    {
        return view('video.submit');
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'src' => ['required', 'regex:/^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$/'],
            'tags.*' => 'required|max:255'
        ]);
        $validator->setAttributeNames([
            'src' => 'آدرس ویدیو', 
            'tags' => 'برچسب'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = auth()->user()->id;

        $youtube = new Youtube();
        $youtube->setPath( public_path('video') );
        $youtube->setVideo( $request->input('src') );
        $youtube->setSuffix( uniqid($user_id) );
        $youtube->setThumbMode( true );
        $youtube->setThumbPath( public_path('thumbnail') );

        $upload = $youtube->download();

        if( $upload > 0 || is_string($upload) ){
            return redirect()->back()->with('message', is_string($upload)?$upload:$youtube->getError($upload));
        }

        $info = $youtube->getInfo();

        $fields = [
            'title' => $info->title,
            'source_url' => $request->input('src'),
            'duration' => null,
            'filename' => $info->filename,
            'counter' => 0
        ];


        $video = new Video($fields);
        $video->user_id = $user_id;
        $video->save();

        $tagNames = $request->input('tags');
        if( $tagNames  ){
            $tagIds = [];
            foreach($tagNames as $tagName)
            {
                $tag = Tag::firstOrCreate(['title'=>$tagName, 'slug'=> str_slug($tagName, '-') ]);
                if($tag)
                {
                    $tagIds[] = $tag->id;
                }
            }
            $video->tags()->sync($tagIds);
        }

        return redirect()->back()->with('message', 'ویدیو شما با موفقیت ثبت شد');
    }
}
