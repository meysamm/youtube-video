@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h1><i class="fa fa-bullhorn"></i> آخرین ویدیوها</h1></div>
            @foreach($last as $row)
                <div class="col-md-3  margin-bottom-15">
                    @include('video._item')
                </div>
            @endforeach
        </div>
        <div class="seperator"></div>
        <div class="row">
            <div class="col-md-12"><h2><i class="fa fa-bookmark"></i> ویدیوهای پربازدید</h2></div>
            @foreach($top as $row)
                <div class="col-md-3 margin-bottom-15">
                    @include('video._item')
                </div>
            @endforeach
        </div>
    </div>
@endsection
