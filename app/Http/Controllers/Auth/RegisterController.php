<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required|in:male,female',
            'birthday' => 'required|regex:/^[\d]{4}\/[\d]{1,2}\/[\d]{1,2}$/u',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => str_random(40)
        ]);
    }


    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $confirmation_code = $user->confirmation_code;
        Mail::send('email.verify', compact('confirmation_code'), function($message) use ($user) {
            $message->to($user->email, $user->fullNmae())
                ->subject('فعال سازی حساب کاربری');
        });

        $this->guard()->logout();
        $request->session()->flash('message', 'لطفا ایمیل خود را چک کنید و برروی ایمیل فعال سازی کلیک کنید.');
        $request->session()->flash('alert-class', 'alert-danger');
        return redirect('login');
    }



    /**
     * confirm a user
     *
     * @param string $confirmation_code)
     * @return void
     */
    public function confirm($confirmation_code, Request $request)
    {
        if( ! $confirmation_code)
        {
            abort(404);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();
        if ( ! $user)
        {
            abort(404);
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        $request->session()->flash('message', 'حساب کاربری شما با موفقیت تایید شد.');
        $request->session()->flash('alert-class', 'alert-success');

        $this->guard()->login($user);
        return Redirect($this->redirectPath());
    }
}
