<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        DB::table('users')->insert([
            'first_name' => 'میثم',
            'last_name' => 'محمودی',
            'gender' => 'male',
            'birthday' => '1369/8/10',
            'email' => 'info@meysam.biz',
            'password' => bcrypt('meysam123456'),
            'confirmed' => 1,
            'created_at' => new \Carbon\Carbon,
            'updated_at' =>new \Carbon\Carbon,
        ]);
    }
}
