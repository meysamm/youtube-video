@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if( count($errors) )
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul>
                            <li>{!! \Session::get('message') !!}</li>
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-info">
                    <ul>
                        <li>لطفا در وارد کردن آدرس ویدیو دقت فرمایید.</li>
                        <li>حداکثر حجم مجاز برای دانلود 200 مگابایت می باشد.</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <form action="" method="post" class="margin-bottom-15">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">آدرس ویدیو در یوتیوب</label>
                        <input type="text" class="form-control" name="src"  placeholder="eg: https://www.youtube.com/watch?v=AQ4ShXC5NBA">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">برچسب ها</label>
                        <select class="form-control tags" name="tags[]"  multiple="multiple" placeholder="برچسب ها را با دکمه اینتر جدا کنید">
                            <option></option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">ثبت ویدیو</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('after_footer')
    <link href="{{ url('plugins/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ url('plugins/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet" />
    <script src="{{ url('plugins/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".tags").select2({
            tags: true,
            dir: "rtl",
            theme: "bootstrap",
            placeholder: 'برچسب ها را با "Enter" جدا کنید',
            minimumResultsForSearch: Infinity
        });
        $("#select2Id").select2 ('container').find ('.select2-search').addClass ('hidden') ;
    });
</script>
@endsection
