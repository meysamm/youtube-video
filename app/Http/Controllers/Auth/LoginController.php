<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if ( $user->confirmed == '0' ) {
            \Log::info(1);
            $this->guard()->logout();
            \Log::info(2);
            $request->session()->flash('message', 'حساب کاربری شما فعال نشده است. لطفا ابتدا حساب کاربری خود را فعال کنید.');
            $request->session()->flash('alert-class', 'alert-danger');
            \Log::info(3);
            return redirect('login');
        }
    }


    public function getActivation()
    {
        return view('auth.activation');
    }

    public function postActivation(Request $request)
    {
        $this->validate($request, ['email' => 'required|string|email|max:255|exists:users'] );

        $email = $request->input('email');
        $user = User::whereEmail($email)->first();

        if( $user->confirmation_code == 1 ){
            return redirect()->back()->with('message', 'حساب کاربری شما قبلا فعال شده است');
        }

        $confirmation_code = $user->confirmation_code;
        Mail::send('email.verify', compact('confirmation_code'), function($message) use ($user) {
            $message->to($user->email, $user->fullNmae())
                ->subject('فعال سازی حساب کاربری');
        });
        return redirect()->back()->with('message', 'ایمیل فعال سازی برای شما ارسال شد. لطفا ایمیل خود را باز کرده و روی لینک فعال سازی کلیک کنید.');
    }


}
