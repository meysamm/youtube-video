<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}"><i class="fa fa-video-camera"></i> ویدیو</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <a class="btn btn-primary pull-left margin-top-10" href="{{ url('submit') }}"><i class="fa fa-cloud-upload"></i> ارسال ویدیو</a>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if( Auth::guest() )
                    <li><a href="{{ route('login') }}">ورود</a></li>
                    <li><a href="{{ route('register') }}">عضویت</a></li>
                    <li><a href="{{ url('list') }}">لیست ویدیوها</a></li>
                @else
                    <li><a href="{{ url('list') }}">لیست ویدیوها</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->first_name.' '.Auth::user()->last_name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    خروج از سایت
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>