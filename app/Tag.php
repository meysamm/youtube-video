<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['title', 'slug'];

    public function videos()
    {
        return $this->belongsToMany(Video::class)->orderBy('id', 'desc');
    }
}
