@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @include('video._sidebar')
            </div>
            <div class="col-md-8">
                <div class="detail-title"><h1>{{ $row->title }}</h1></div>
                <div class="detail-video margin-top-15">
                    <video controls>
                        <source src="{{ url("video/$row->filename") }}" type="video/mp4">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
                <div class="detail-tags">
                    <div class="pull-right margin-left-15 counter">بازدید: {{ $row->counter }}</div>
                    <div class="pull-right margin-left-15">برچسب ها: </div>
                    <ul class="pull-right">
                        @foreach($row->tags as $tag)
                            <li><a href="{{ url("list/$tag->slug") }}">{{ $tag->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
