@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @include('video._sidebar')
            </div>
            <div class="col-md-8">
                <div class="list-title"><h1><i class="fa fa-bullhorn"></i> آخرین ویدیوها</h1></div>
                <div class="list-body margin-top-15">
                    @foreach($last as $row)
                        <div class="col-md-4 margin-bottom-15">
                            @include('video._item')
                        </div>
                    @endforeach
                </div>
                <div class="pagination">
                    {{ $last->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
