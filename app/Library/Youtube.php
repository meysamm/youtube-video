<?php
/**
 * Created by PhpStorm.
 * User: Meysam
 * Date: 28-09-2017
 * Time: 10:16 AM
 */

namespace App\Library;


class Youtube
{

    private $video_id;
    private $video_src;
    private $video_info;
    private $thumb_mode = false;
    private $thumb_path;
    private $thumb_url;
    private $thumb_filename;
    private $filename;
    private $file_path;
    private $file_size;

    private $max_file_size = 209715200; // 200mb
    private $suffix = '';

    public function __construct( $url = null, $file_path = null)
    {
        if( !is_null($url) ){
            $this->setVideo($url);
        }

        if( !is_null($file_path) ){
            $this->setPath($file_path);
        }
    }

    public function setVideo($url)
    {
        $this->video_src = $url;
        $this->video_id = $this->getVideoIdFromUrl($url);
    }

    public function setPath($file_path)
    {
        $this->file_path = $file_path;
    }

    public function setThumbMode( $mode = true )
    {
        $this->thumb_mode = $mode;
    }

    public function setThumbPath($thumb_path)
    {
        $this->thumb_path = $thumb_path;
    }

    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;
    }

    public function setMaxFileSize($size)
    {
        $this->max_file_size = $size;
    }

    /**
     * execute program
     *
     * @return int|string zero = success, none-zero = failure
     */
    public function  download()
    {
        try{
            $this->video_info = $this->getVideoInfo();
            if( is_null($this->video_info) ){
                return 11;
            }

            $this->video_src = $this->getVideoSrc($this->video_info);
            if( is_null($this->video_src) ){
                return 12;
            }

            $this->file_size = $this->getFileSize($this->video_src);
            if( $this->file_size == -1 || $this->file_size > $this->max_file_size ){
                return 40;
            }

            //$this->filename = $this->getFileNameFromUrl($this->video_src);
            $this->filename = $this->makeUniqueFileName($this->video_info['title']);

            if( $this->thumb_mode == true ){
                if( $this->downloadThumb() === false ){
                    return 30;
                }
            }

            return $this->downloadVideo();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * make a unique filename for a video
     * @param string $title
     * @return string
     */
    public function makeUniqueFileName($title)
    {
        return $this->cleanFileName($title).'_'.$this->suffix.'.mp4';
    }

    /**
     * get all variables in result
     *
     * @return object
     */
    public function getInfo()
    {
        return (object)[
            'video_id' => $this->video_id,
            'video_src' => $this->video_src,
            'video_info' => $this->video_info,
            'filename' => $this->filename,
            'file_path' => $this->file_path,
            'title' => $this->video_info['title'],
            'duration' => '',
            'thumbnail' => ''
        ];
    }

    /**
     * get video info for a video
     *
     * @return array
     */
    public function getVideoInfo()
    {
        $url = 'http://www.youtube.com/get_video_info?video_id='.$this->video_id;
        $info = [];
        parse_str(file_get_contents($url), $info);
        return $info;
    }

    /**
     * get source video to download
     *
     * @param array $video_info
     * @return null|string
     */
    public function getVideoSrc(&$video_info)
    {
        $video_src = null;
        $streams = $video_info['url_encoded_fmt_stream_map'];
        $streams = explode(',',$streams);

        foreach ($streams as $streamdata) {
            parse_str($streamdata,$streamdata);

            foreach ($streamdata as $key => $value) {
                if ($key == "url") {
                    $video_src = urldecode($value);
                    break 2; // break from 2 loops
                }
            }
        }
        return $video_src;
    }

    /**
     * remove all none-ascii characters from file
     *
     * @param string $filename
     * @return string
     */
    protected function cleanFileName($filename)
    {
        $filename = preg_replace('/[^a-zA-Z0-9\-\._]/','', str_replace(' ', '_', trim($filename)));
        return $filename;
    }

    public function downloadThumb()
    {
        $url = 'https://i.ytimg.com/vi/' . $this->video_id . '/sddefault.jpg';
        $source = file_get_contents($url);
        $filename = str_replace('.mp4', '.jpg', $this->filename);
        return file_put_contents($this->thumb_path.DIRECTORY_SEPARATOR.$filename, $source);
    }

    /**
     * @return int zero=success , none zero failure
     */
    private function downloadVideo()
    {
        $ch = curl_init();
        set_time_limit ( 300 ); // 5 minutes

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);        // Get returned value as string (don't put to screen)
        curl_setopt($ch, CURLOPT_USERAGENT, $this->getUserAgent()); // Spoof the user agent
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_URL, $this->video_src );

        $return_code = 0;

        $source = curl_exec($ch);
        $response_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        if( $response_code !== 200 ){
            $return_code = 21;
        }
        else{
            $put = file_put_contents($this->file_path.DIRECTORY_SEPARATOR.$this->filename, $source);
            if( $put=== false){
                $return_code = 22;
            }
        }
        return $return_code;
    }

    /**
     * get error message for a response code
     *
     * @param $code
     * @return mixed
     */
    public function getError($code)
    {
        $errors = [
            11 => 'مشکلی در دریافت اطلاعات ویدیو بوجود آمده است',
            12 => 'سورسی برای دانلود ویدیو یافت نشد',
            21 => ' پاسخی از سرور برای دانلود ویدیو دریافت نشد',
            22 => 'خطایی در زمان نوشتن فایل روی دیسک بوجود آمده است',
            30 => 'دانلود عکس بندانگشتی با مشکل مواجه شده است',
            40 => 'حجم ویدیوی درخواستی شما بیش از مقدار مجاز است'
        ];
        return isset($errors[$code])?$errors[$code]:null;
    }

    /**
     * Cuts video id from absolute Youtube or youtu.be video url
     *
     * @param  string $videoUrl Full Youtube or youtu.be video url
     * @return string           Video ID
     */
    protected function getVideoIdFromUrl($videoUrl)
    {
        $videoId = $videoUrl;
        $urlPart = parse_url($videoUrl);
        $path = $urlPart['path'];
        if (isset($urlPart['host']) && strtolower($urlPart['host']) == 'youtu.be') {
            if (preg_match('/\/([^\/\?]*)/i', $path, $temp))
                $videoId = $temp[1];
        } else {
            if (preg_match('/\/embed\/([^\/\?]*)/i', $path, $temp))
                $videoId = $temp[1];
            elseif (preg_match('/\/v\/([^\/\?]*)/i', $path, $temp))
                $videoId = $temp[1];
            elseif (preg_match('/\/watch/i', $path, $temp) && isset($urlPart['query']))
            {
                parse_str($urlPart['query'], $query);
                $videoId = $query['v'];
            }
        }
        return $videoId;
    }


    /**
     * Returns the size of a file without downloading it, or -1 if the file
     * size could not be determined.
     *
     * @param $url - The location of the remote file to download. Cannot
     * be null or empty.
     *
     * @return The size of the file referenced by $url, or -1 if the size
     * could not be determined.
     */
    protected function getFileSize( $url ) {
        // Assume failure.
        $result = -1;

        $curl = curl_init( $url );

        // Issue a HEAD request and follow any redirects.
        curl_setopt( $curl, CURLOPT_NOBODY, true );
        curl_setopt( $curl, CURLOPT_HEADER, true );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt( $curl, CURLOPT_USERAGENT, $this->getUserAgent() );

        $data = curl_exec( $curl );
        curl_close( $curl );

        if( $data ) {
            $content_length = "unknown";
            $status = "unknown";

            if( preg_match( "/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches ) ) {
                $status = (int)$matches[1];
            }

            if( preg_match( "/Content-Length: (\d+)/", $data, $matches ) ) {
                $content_length = (int)$matches[1];
            }

            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if( $status == 200 || ($status > 300 && $status <= 308) ) {
                $result = $content_length;
            }
        }

        return $result;
    }

    /**
     * get user agnet if doesn't exsits return a user agent
     * @return string
     */
    protected function getUserAgent()
    {
        return $_SERVER['HTTP_USER_AGENT']??'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36';
    }

}