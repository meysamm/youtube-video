<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['user_id', 'title', 'source_url', 'duration', 'filename', 'thumbnail', 'counter'];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
