<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>فعال سازی حساب کاربری</h2>

        <div>
            لطفا برای تایید ایمیل خود برروی لینک زیر کلیک نمایید:<br/>
            {{ URL::to('register/verify/' . $confirmation_code) }}

        </div>

    </body>
</html>