@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">فعال سازی حساب کاربری </div>

                <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('activation') }}">
                        {{ csrf_field() }}


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">ایمیل</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">ثبت اطلاعات</button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="alert alert-info"> لطفا ایمیل خود را وارد کنید و سپس روی لینکی که به ایمیل شما ارسال می شود کلیک کنید</div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


@section('after_footer')
<script src="{{ url('plugins/persianDatepicker/js/jquery-1.10.1.min.js') }}"></script>
<script src="{{ url('plugins/persianDatepicker/js/persianDatepicker.min.js') }}"></script>
<link rel="stylesheet" href="{{ url('plugins/persianDatepicker/css/persianDatepicker-default.css') }}">

<script type="text/javascript">
    $(function() {
        $("#birthday").persianDatepicker();
    });
</script>
@endsection