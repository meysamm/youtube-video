@php $thumb = str_replace('.mp4', '.jpg', $row->filename); @endphp
<a class="item" href="{{ url("video/$row->id") }}">
    <div class="img">
        <img src="{{ url("thumbnail/$thumb") }}" class="img-responsive" />
    </div>
    <div class="title">{{ $row->title }}</div>
</a>