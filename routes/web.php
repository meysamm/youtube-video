<?php
use Masih\YoutubeDownloader\YoutubeDownloader;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function () {
    return time() / 1000;
});

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('register/verify/{confirmationCode}', 'Auth\RegisterController@confirm')->name('confirmation_path');
Route::get('activation', 'Auth\LoginController@getActivation')->name('activation');
Route::post('activation', 'Auth\LoginController@postActivation');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'VideoController@index');
Route::get('video/{id}', 'VideoController@detail');
Route::get('list/{tag?}', 'VideoController@list');

Route::group(['middleware' => ['auth']], function () {
    Route::get('submit', 'VideoController@submit');
    Route::post('submit', 'VideoController@store');
});