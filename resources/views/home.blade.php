@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">پنل کاربری</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>شما وارد حساب کاربری خود شدید.</p>
                    <div>
                        <a href="{{ url('submit') }}" class="btn btn-primary">ثبت ویدیو جدید</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
