@inject('video', 'App\Video')

<div class="side-title"><h3><i class="fa fa-bookmark"></i> ویدیوهای پربازدید</h3></div>
<div class="side-body">
    <ul>
        @foreach($video->orderBy('counter', 'desc')->take(5)->get() as $row)
            @php $thumb = str_replace('.mp4', '.jpg', $row->filename); @endphp
            <li>
                <div class="img">
                    <a href="{{ url("video/$row->id") }}">
                        <img src="{{ url("thumbnail/$thumb") }}" class="img-responsive" />
                    </a>
                </div>
                <div class="text"><a href="{{ url("video/$row->id") }}">{{ $row->title }}</a></div>
            </li>
        @endforeach
    </ul>
</div>